# zenoh C client example

Here some examples have been imported from [Zenoh C](https://github.com/eclipse-zenoh/zenoh-c/). The CMake build creates an executable for each example and installs them into the binary directory of the system.

In order to build this, the Zenoh shared library and include files are required by cmake.

This repository was created to test the [zenoh-c yocto recipe](https://booting.oniroproject.org/distro/oniro/-/merge_requests/533).

To integrate this repository in a Yocto recipe and install the zenoh examples in your image, add the following recipe:
```
# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: Apache-2.0

SUMMARY = "Zenoh C examples"
DESCRIPTION = "Examples imported from zenoh-c showing usage of the Zenoh C client library"
LICENSE = "Apache-2.0"

SRC_OPT_PROTO = "protocol=https"
SRC_OPT_BRANCH = "branch=main"
SRC_OPT_CLONE_DIR = "git/apps/zenoh-c-examples"
SRC_OPT_DEST = "destsuffix=${SRC_OPT_CLONE_DIR}"

SRC_OPTIONS = "${SRC_OPT_PROTO};${SRC_OPT_DEST};${SRC_OPT_BRANCH}"
SRC_URI += "git://booting.oniroproject.org/francesco.pham/zenoh-c-client-example.git;${SRC_OPTIONS}"

SRCREV = "3d6decb120b0265e21187c5ecd2277a9a13e23d2"

S = "${WORKDIR}/${SRC_OPT_CLONE_DIR}"

LIC_FILES_CHKSUM = "file://${S}/LICENSE;md5=ba963850f6731c74878fe839d227e675"

inherit cmake pkgconfig

DEPENDS:append = "\
	zenoh-c \
    "

RDEPENDS:${PN}:append = "\
	zenoh-c \
    "
```
